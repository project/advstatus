Introduction
============

The Advanced status report module provides a detailed status report of
your site. This report may be useful when debugging issues with your
site, or when asking for technical support from thirdy-parties, such
as Drupal.org issue queues, or Drupal expert individuals or
companies. The Advanced status report can be viewed on the your site
or be downloaded to be sent in email attachments or uploaded
elsewhere.

* For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/flaviovs/2426407

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2426407


Requirements
============

None.


Installation
============

Install as you would normally install a contributed Drupal
module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.


Configuration
=============

Configure user permission in Administration » People » Permissions:

* *System » Administer site configuration* -- give this permission to user
  roles that should be allowed to view and/or download the Advanced
  status report.


Usage
=====

Go to *Reports » Status report » Advanced status report* to view and
optionally download the report.


Author/Maintainers
==================

Flávio Veloso <flaviovs at magnux dot com>
