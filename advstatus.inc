<?php
/**
 * @file
 * Main APIs and helper functions.
 */

/**
 * Anonymize a filename.
 *
 * Used to to hide local details from file names.
 */
function advstatus_anon_file($name) {
  $droot = realpath(DRUPAL_ROOT);
  return preg_replace(array(
                        '/^' . preg_quote($droot, '/') . '/',
                        '~/sites/(?!(?:all|default))/~',
                        '~^/.*/sites/(all|modules|\[SITENAME\])/~',
                      ),
                      array(
                        '[DRUPAL_ROOT]',
                        '/sites/[SITENAME]/',
                        '[DRUPAL_ROOT]/sites/$1/',
                      ),
                      realpath($name));
}


/**
 * Get a sequential MD Extra footnote.
 *
 * Used to generate guaranteed unique report-wide Markdown Extra
 * footnotes.
 */
function advstatus_get_footnote() {
  static $fn = 0;
  static $chars = array('*', '†', '‡', '§', '‖', '¶');
  static $nr_chars;

  if (!$nr_chars) {
    $nr_chars = count($chars);
  }

  $selected = $fn % $nr_chars;
  $repeat = floor($fn / $nr_chars);
  $id = str_repeat($chars[$selected], $repeat + 1);

  $fn++;

  return "[^$id]";
}


/**
 * Return magnitude range of numbers.
 *
 * Used to hide exact numbers in the reports.
 */
function advstatus_mrange($n, $max = 100000) {

  if ($n == 0) {
    return "0";
  }
  else if ($n == 1) {
    return "1";
  }
  else if ($n < 10) {
    return "< 10";
  }
  else if ($n >= $max) {
    return ">= $max";
  }

  $m = floor(log10($n));
  $l = pow(10, $m);
  $h = pow(10, $m + 1);

  return "$l - $h";
}


/**
 * Provides a service for anonymizing data prior to being output.
 */
class AdvStatusAnonymizer {
  public $names = array();
  public $common = array();
  public $count = array();

  /**
   * Adds "common" names, so that they don't get anonymized.
   *
   * Use this method to add common names that should/need not to be
   * anonymized. For example, the "node" entity is a Drupal core
   * standard, so there is no need to anonymize it in the report (BTW,
   * this is taken care on modules/node.inc).
   *
   * @param string $class
   *   The "class" of string not to be anonymized.
   * @param mixed $commons
   *   The common string(s) in the class that should not be
   *   anonymized. Can be an string or an array of strings.
   */
  public function addCommon($class, $commons) {
    if (!is_array($commons)) {
      $commons = array($commons);
    }
    if (!isset($this->common[$class])) {
      $this->common[$class] = array();
    }
    foreach ($commons as $c) {
      $this->common[$class][] = $c;
    }
  }

  /**
   * Return and anonymized string of an specified class.
   *
   * @param string $class
   *   The "class" of the string.
   * @param string $name
   *   The string to be anonymized.
   */
  public function get($class, $name) {
    if (isset($this->common[$class]) && in_array($name, $this->common[$class])) {
      return $name;
    }
    if (!isset($this->names[$class])) {
      $this->names[$class] = array();
      $this->count[$class] = 0;
    }

    if (!isset($this->names[$class][$name])) {
      $this->count[$class]++;
      $this->names[$class][$name] = $class . $this->count[$class];
    }

    return $this->names[$class][$name];
  }

}


/**
 * An internal helper class to be used when converting HTML to MD-Extra.
 */
class AdvStatusHTML2MDHelper {
  public $footnotes = array();

  /**
   * Convert link matches to Markdown syntax.
   */
  public function convertLinks($matches) {
    $url = $matches[1];
    $text = $matches[2];

    if (valid_url($url, TRUE)) {
      $out = "[$text]($url)";
    }
    else {
      $fn = advstatus_get_footnote();
      $out = $text . $fn;
      $this->footnotes[] = "$fn: " . t("Link to @url",
                                       array('@url' => "`$url`"));
    }

    return $out;
  }

}


/**
 * Convert a piece of HTML text to Markdown Extra.
 *
 * @param string $string
 *   The string containing HTML text.
 * @param AdvStatusHTML2MDHelper $helper
 *   (optional) And AdvStatusHTML2MDHelper() helper object.
 */
function advstatus_html2md($string, AdvStatusHTML2MDHelper $helper = NULL) {

  if (!$helper) {
    $helper = new AdvStatusHTML2MDHelper();
  }

  $out = filter_xss(rtrim($string), array('a', 'em', 'strong'));
  $out = preg_replace(array(
                        '~<em\b.*?>\s*~',
                        '~\s*</em>~',
                        '~<strong\b.*?>\s*~',
                        '~\s*</strong>~',
                        ),
                      array(
                        '*',
                        '*',
                        '**',
                        '**',
                        ),
                      $out);
  $out = preg_replace_callback('~<a.*?href="(.+)?".*?>(.+?)</a>~',
                               array($helper, 'convertLinks'),
                               $out);
  return $out;
}


/**
 * A str_pad() replacement that correctly handles multi-byte strings.
 */
function _advstatus_str_pad($str, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT) {

  $str_len = drupal_strlen($str);
  $padding = ($pad_len - $str_len);
  if ($padding <= 0) {
    // Nothing to do.
    return $str;
  }
  switch ($dir) {
    case STR_PAD_LEFT:
      $out = str_repeat($pad_str, $padding) . $str;
      break;

    case STR_PAD_RIGHT:
      $out = $str . str_repeat($pad_str, $padding);
      break;

    case STR_PAD_BOTH:
      $padding_left = floor($padding / 2);
      $padding_right = $padding - $padding_left;
      $out = str_repeat($pad_str, $padding_left)
        . $str
        . str_repeat($pad_str, $padding_right);
      break;

    default:
      throw new InvalidArgumentException('Invalid padding');
  }
  return $out;
}


/**
 * Represents a node in the report object hierarchy.
 *
 * NB: The "Node" in this class name has NOTHING to do with Drupal
 * nodes. It merely refers to nodes on a tree structure.
 */
class AdvStatusNode {
  public $parent;
  public $children = array();
  public $separator = '';


  /**
   * Add a child node.
   */
  public function addChild(AdvStatusNode $child) {
    $child->parent = $this;
    $this->children[] = $child;
  }


  /**
   * Conveninent function for adding text to nodes.
   */
  public function add($element) {
    if (is_string($element)) {
      $element = new AdvStatusReportText($element);
    }

    $this->addChild($element);
  }


  /**
   * Destroy children and remove references to them.
   *
   * This is needed so that PHP can garbage collect unreferenced
   * objects. Usually, only one report is displayed on a single HTTP
   * request, so memory leaks due to unreachable objects are not an
   * issue, but this is not an excuse for not doing rhe right
   * thing.
   */
  public function destroy() {
    foreach ($this->children as $child) {
      $child->destroy();
    }
    $this->children = array();
  }


  /**
   * Return the depth of this node in the report node hierarchy.
   */
  public function depth($class = NULL) {
    $depth = 0;
    $parent = $this->parent;
    while ($parent) {
      if (!$class || is_a($parent, $class)) {
        $depth++;
      }
      $parent = $parent->parent;
    }
    return $depth;
  }


  /**
   * Return a string representation of this object.
   */
  public function __toString() {
    return implode($this->separator, $this->children);
  }


  /**
   * Indicates if this is a leaf node (i.e. has no children).
   */
  public function isLeaf() {
    return !((bool) $this->children);
  }

}


/**
 * Represents a report node that contains ordinary text.
 */
class AdvStatusReportText extends AdvStatusNode {
  public $text = '';

  /**
   * Construct the object.
   */
  public function __construct($text) {
    $this->text = $text;
  }


  /**
   * Return a string representation of this object.
   */
  public function __toString() {
    return $this->text . parent::__toString() . "\n";
  }

}


/**
 * Represents a report node that contains pre-formatted (code) text.
 */
class AdvStatusReportCode extends AdvStatusReportText {

  /**
   * Return a string representation of this object.
   */
  public function __toString() {
    return "\n" . preg_replace('/^/m', "     ", trim(parent::__toString())) . "\n";
  }

}


/**
 * Represents a report node list.
 */
class AdvStatusReportList extends AdvStatusNode {

  /**
   * Return a string representation of this object.
   */
  public function __toString() {

    $depth = $this->depth('AdvStatusReportList');

    $indent = str_repeat('  ', $depth);

    switch ($depth % 3) {
      case 0:
        $prefix = '*   ';
        break;

      case 1:
        $prefix = '-   ';
        break;

      case 2:
        $prefix = '+   ';
        break;
    }

    $out = "\n";
    foreach ($this->children as $child) {
      $out .= "$indent$prefix ";

      $child_out = rtrim((string) $child);

      $child_out = preg_replace('/\n/m', "\n     " . $indent, $child_out);

      $out .= $child_out . "\n";
    }

    return $out;
  }


  /**
   * Add a "label: value" to a list.
   *
   * @todo Remove.
   */
  public function addValue($label, $value) {
    assert('is_scalar($value)');
    $label_len = drupal_strlen($label);
    if ($label_len < 20) {
      $label .= str_repeat('.', 20 - $label_len);
    }
    $this->add("$label: $value");
  }


  /**
   * Add a "variable" value to the list.
   *
   * @todo Replace by an "text variable" node?
   */
  public function addVariable($label, $value) {
    ob_start();
    // The var_dump() below is NOT a leftover from debugging.
    // @ignore production_php
    var_dump($value);
    $dump = rtrim(ob_get_clean());

    if ($value === NULL || is_scalar($value)) {
      $this->add("$label: ``$dump``");
    }
    else {
      $this->add("$label:\n\n" . preg_replace('/^/m', "\t", $dump)) . "\n";
    }
  }

}


/**
 * Represents a row in a report node table.
 */
class AdvStatusReportTableRow extends AdvStatusNode {

}


/**
 * Represents a table in a report node hierarchy.
 */
class AdvStatusReportTable extends AdvStatusReport {
  public $headers = array();
  protected $colWidth = array();
  protected $align = array();
  protected $padDir = array();

  /**
   * Add a table header.
   */
  public function addHeader($header, $align = NULL) {
    if (!$this->isLeaf()) {
      throw new InvalidArgumentException('Cannot add header to table with data');
    }

    $this->headers[] = $header;
    $this->colWidth[] = drupal_strlen($header);
    $this->align[] = $align;
    switch ($align) {
      case NULL:
      case 'l':
        $this->padDir[] = STR_PAD_RIGHT;
        break;

      case 'r':
        $this->padDir[] = STR_PAD_LEFT;
        break;

      case 'c':
        $this->padDir[] = STR_PAD_BOTH;
        break;

      default:
        throw new InvalidArgumentException("Invalid alignment value '$align'");
    }
  }


  /**
   * Add a row of cells to the table.
   */
  public function add($cells) {
    $row = new AdvStatusReportTableRow();
    $nr_cells = count($cells);
    if ($nr_cells != count($this->headers)) {
      throw new InvalidArgumentException('Number of cells does not match header');
    }

    for ($i = 0; $i < $nr_cells; $i++) {
      $cell = (string) $cells[$i];
      $row->add($cell);
      $this->colWidth[$i] = max($this->colWidth[$i], drupal_strlen($cell));
    }

    $this->addChild($row);
  }


  /**
   * Output a single row.
   */
  protected function outputRow($row, $nr_cells, $pad = ' ', $pad_dir = NULL) {

    if (!$pad_dir) {
      $pad_dir = $this->padDir;
    }

    $out = '';

    if ($nr_cells == 1) {
      $out .= '|';
    }

    for ($i = 0; $i < $nr_cells; $i++) {

      if ($i) {
        $out .= '| ';
      }

      $width = $this->colWidth[$i] + 2;

      $out .= _advstatus_str_pad(trim($row[$i]),
                                  $width,
                                  $pad,
                                  $pad_dir[$i]);

      $out .= ' ';

      if ($nr_cells == 1) {
        $out .= '|';
      }
    }

    return $out;
  }


  /**
   * Return a string representation of this object.
   */
  public function __toString() {

    if ($this->isLeaf()) {
      // No rows -- nothing to do.
      return '';
    }

    $out = '';

    if ($this->title) {
      $out .= $this->getTitle();
    }

    // There must be an empty line before a MD table.
    $out .= "\n";

    $nr_cells = count($this->headers);

    $hpad = array_fill(0, $nr_cells, STR_PAD_RIGHT);
    $out .= $this->outputRow($this->headers, $nr_cells, ' ', $hpad);
    $out .= "\n";

    $dashes = array();
    for ($i = 0; $i < $nr_cells; $i++) {
      $dashes[] = ($this->align[$i] ? ':' : '');
    }
    $out .= $this->outputRow($dashes, $nr_cells, '-');

    $out .= "\n";

    foreach ($this->children as $row) {
      $out .= $this->outputRow($row->children, $nr_cells);
      $out .= "\n";
    }

    return $out;
  }

}


/**
 * Represents a complete report.
 */
class AdvStatusReport extends AdvStatusNode {
  public $title;
  public $anonymizer;

  /**
   * Construct the object.
   */
  public function __construct($title = NULL) {
    $this->title = $title;
    $this->anonymizer = new AdvStatusAnonymizer();
    $this->separator = "\n";
  }


  /**
   * Return the object title, Markdown Extra properly formatted.
   */
  protected function getTitle() {
    if (!$this->title) {
      return '';
    }

    $out = '';

    $title_len = drupal_strlen($this->title);
    $depth = $this->depth('AdvStatusReport');

    switch ($depth) {
      case 0:
        $out .= $this->title . "\n" . str_repeat('=', $title_len);
        break;

      case 1:
        $out .= "\n" . $this->title . "\n" . str_repeat('-', $title_len);
        break;

      default:
        $hashes = str_repeat('#', $depth + 1);
        $out .= "\n" . $hashes . ' ' . $this->title . ' ' . $hashes;
        break;
    }

    $out .= "\n";

    return $out;
  }


  /**
   * Return a string representation of this object.
   */
  public function __toString() {
    $out = '';

    if ($this->title) {
      $out .= $this->getTitle();
    }

    $out .= parent::__toString();

    return $out;
  }

}


/**
 * Return a string representation of the current PHP "error_reporting" value.
 */
function _advstatus_php_error_reporting() {

  $errors = array(
    E_ERROR => 'E_ERROR',
    E_WARNING => 'E_WARNING',
    E_PARSE => 'E_PARSE',
    E_NOTICE => 'E_NOTICE',
    E_CORE_ERROR => 'E_CORE_ERROR',
    E_CORE_WARNING => 'E_CORE_WARNING',
    E_COMPILE_ERROR => 'E_COMPILE_ERROR',
    E_COMPILE_WARNING => 'E_COMPILE_WARNING',
    E_USER_ERROR => 'E_USER_ERROR',
    E_USER_WARNING => 'E_USER_WARNING',
    E_USER_NOTICE => 'E_USER_NOTICE',
  );

  if (defined('E_DEPRECATED')) {
    $errors[E_DEPRECATED] = 'E_DEPRECATED';
  }

  if (defined('E_STRICT')) {
    $errors[E_STRICT] = 'E_STRICT';
  }

  if (defined('E_RECOVERABLE_ERROR')) {
    $errors[E_RECOVERABLE_ERROR] = 'E_RECOVERABLE_ERROR';
  }

  $er = error_reporting();

  $out = array();
  $inc = PHP_INT_MAX;
  if (($er & E_ALL) == E_ALL) {
    $out[] = 'E_ALL';
    $inc &= ~E_ALL;
  }
  else {
    $inc = 0;
  }

  foreach ($errors as $err => $name) {
    if (($inc & $err) == $err) {
      $out[] = $name;
    }
  }

  return implode(' | ', $out);
}


/**
 * Return the requirements report.
 */
function _advstatus_report_requirements() {

  include_once DRUPAL_ROOT . '/includes/install.inc';
  drupal_load_updates();
  $requirements = module_invoke_all('requirements', 'runtime');

  $report = new AdvStatusReport(t('System requirements'));

  $table = new AdvStatusReportTable();
  $table->addHeader(t('Item'));
  $table->addHeader(t('Status'));

  $helper = new AdvStatusHTML2MDHelper();

  foreach ($requirements as $name => $req) {

    // Skip information reported elsewhere.
    if ($name == 'drupal' || $name == 'webserver' || $name == 'php'
        || $name == 'database_system' || $name == 'database_system_version'
        || $name == 'php_memory_limit' || $name == 'php_register_globals') {
      continue;
    }

    $value = advstatus_html2md($req['value'], $helper);

    if (isset($req['severity'])) {
      switch ($req['severity']) {
        case REQUIREMENT_WARNING:
          $value .= " (**WARNING**)";
          break;

        case REQUIREMENT_ERROR:
          $value .= " (**ERROR**)";
          break;
      }
    }

    $table->add(array($req['title'], $value));
  }

  $report->addChild($table);

  if ($helper->footnotes) {
    $report->add("\n");
    foreach ($helper->footnotes as $fn) {
      $report->add("   $fn");
    }
  }

  return $report;
}


/**
 * Return the themes report.
 */
function _advstatus_report_themes() {

  $report = new AdvStatusReport(t('Themes'));

  $list = new AdvStatusReportList();

  $themes_info = system_get_info('theme');

  $theme_default = variable_get('theme_default', '');

  $fn = advstatus_get_footnote();

  foreach ($themes_info as $name => $info) {
    $name_column = $name;
    $version = (!empty($info['version']) ? $info['version'] : '*' . t('unknown version') . '*');
    if ($name == $theme_default) {
      $version .= " $fn";
    }
    $list->add("$name_column $version");
  }

  $report->addChild($list);

  $report->add("\n   $fn: Default theme");

  return $report;
}


/**
 * Return the modules report.
 */
function _advstatus_report_modules() {

  $report = new AdvStatusReport(t('Modules'));

  $list = new AdvStatusReportList();

  $modules_info = system_get_info('module');

  foreach ($modules_info as $name => $info) {
    $version = (!empty($info['version']) ?
                $info['version'] : '*' . t('unknown version') . '*');
    $list->add("$name $version");
  }

  $report->addChild($list);

  return $report;
}


/**
 * Return the PHP report.
 */
function _advstatus_report_php() {

  $report = new AdvStatusReport(t('PHP'));

  $table = new AdvStatusReportTable();

  $table->addHeader(t('Item'));
  $table->addHeader(t('Value'));

  // Memory limit.
  $value = ini_get('memory_limit');
  $table->add(array(
                t('Memory limit'),
                $value == -1 ? t('-1 (Unlimited)') : $value,
              ));

  // Register globals.
  $value = trim(ini_get('register_globals'));
  if ($value && strtolower($value) != 'off') {
    $res = t("Enabled ('@value')", array('@value' => $value));
  }
  else {
    $res = t('Disabled');
  }
  $table->add(array(t('Register globals'), $res));

  // Error reporting.
  $table->add(array(t('Error reporting'), _advstatus_php_error_reporting()));

  // Maximum input time.
  $value = ini_get('max_input_time');
  $table->add(array(
                t('Maximum input parsing time'),
                $value == -1 ? t('-1 (Unlimited)') :
                format_plural($value, '@count second', '@count seconds'),
              ));

  // Output buffering.
  $value = ini_get('output_buffering');
  $table->add(array(
                t('Output buffering'),
                $value ? format_plural($value, '@count byte', '@count bytes') : t('Off'),
              ));

  // Maximum execution time.
  $value = ini_get('max_execution_time');
  $table->add(array(
                t('Maximum execution time'),
                format_plural($value, '@count second', '@count seconds'),
              ));

  // POST max size.
  $table->add(array(
                t('POST maximum size'),
                ini_get('post_max_size'),
              ));

  // File uploads.
  $table->add(array(
                t('File uploads'),
                ini_get('file_uploads') ? t('On') : t('Off'),
              ));

  // Maximum size for uploaded files.
  $table->add(array(
                t('Maximum size for uploaded files'),
                ini_get('upload_max_filesize'),
              ));

  // Maximum number of simultaneous uploads.
  $table->add(array(
                t('Maximum number of simultaneous uploads'),
                ini_get('max_file_uploads'),
              ));

  // Allow opening URLs as files.
  $table->add(array(
                t('Allow opening URLs as files'),
                ini_get('allow_url_fopen') ? t('On') : t('Off'),
              ));

  $report->addChild($table);

  return $report;
}


/**
 * Return the MySQL report.
 */
function _advstatus_report_database_mysql($conn) {

  $report = new AdvStatusReport(t('MySQL information'));

  $res = $conn->query("SHOW VARIABLES")->fetchAllAssoc('Variable_name');

  $table = new AdvStatusReportTable();

  $table->addHeader(t('Setting'));
  $table->addHeader(t('Description'));
  $table->addHeader(t('Value'));

  $table->add(array(
                'max_connections',
                t('Maximum number of connections'),
                $res['max_connections']->Value,
              ));
  $table->add(array(
                'key_buffer',
                t('Key buffer size'),
                format_size($res['key_buffer_size']->Value),
              ));
  $table->add(array(
                'myisam_sort_buffer_size',
                t('MyISAM sort buffer size'),
                format_size($res['myisam_sort_buffer_size']->Value),
              ));
  $table->add(array(
                'join_buffer_size',
                t('JOIN buffer size'),
                format_size($res['join_buffer_size']->Value),
              ));
  $table->add(array(
                'sort_buffer_size',
                t('Sort buffer size'),
                format_size($res['join_buffer_size']->Value),
              ));
  if (isset($res['table_cache'])) {
    // MySQL <= 5.1.2
    $table->add(array(
                  'table_cache',
                  t('Table cache'),
                  format_plural($res['table_cache']->Value,
                                '1 table', '@count tables'),
                ));
  }
  else {
    $table->add(array(
                  'table_open_cache',
                  t('Table cache'),
                  format_plural($res['table_open_cache']->Value,
                                '1 table', '@count tables'),
                ));
  }
  $table->add(array(
                'max_allowed_packet',
                t('Maximum allowed network packet'),
                format_size($res['max_allowed_packet']->Value),
              ));
  $table->add(array(
                'query_cache_limit',
                t('Query cache limit'),
                format_size($res['query_cache_limit']->Value),
              ));
  $table->add(array(
                'Query cache size',
                t('Query cache size'),
                format_size($res['query_cache_size']->Value),
              ));
  $table->add(array(
                'tmp_table_size',
                t('Maximum size of temporary tables'),
                format_size($res['tmp_table_size']->Value),
              ));

  $report->addChild($table);

  $res = $conn->query('SELECT sum(data_length + index_length) as total '
                      . 'FROM information_schema.TABLES')->fetch();

  $report->add(t('Total database size: @size',
                 array('@size' => format_size($res->total))));

  return $report;
}


/**
 * Return a database target report.
 */
function _advstatus_report_database_target($db_name, $tg_name, $target, $anon) {

  $report = new AdvStatusReport();

  $report->title = t('Details for database "@database", target "@target"',
                     array(
                       '@database' => $anon->get('database', $db_name),
                       '@target' => $anon->get('dbtarget', $tg_name),
                     ));

  $list = new AdvStatusReportList();

  $conn = Database::getConnection();
  $driver = $conn->driver();

  $list->add(t('Driver: @driver',
               array('@driver' => $driver)));
  $list->add(t('Host: @host',
               array('@host' => $anon->get('host', $target['host']))));
  $list->add(t('Port: @port',
               array('@port' => (empty($target['port']) ? '*' . t('Not set') . '*' : $target['port']))));
  $list->add(t('User: @user',
               array('@user' => (empty($target['username']) ? '*' . t('Not set') . '*' : $anon->get('dbuser', $target['username'])))));
  $list->add(t('Password protected: @yes_no',
               array('@yes_no' => (empty($target['password']) ? t('No') : t('Yes')))));
  $list->add(t('Server version: @version',
               array('@version' => $conn->version())));
  $list->add(t('Use prefix: @yes_no',
               array('@yes_no' => (empty($target['prefix']) ? t('No') : t('Yes')))));
  $list->add(t('Transactions enabled: @yes_no',
               array('@yes_no' => (empty($target['transactions']) ? t('No') : t('Yes')))));

  $report->addChild($list);

  switch ($driver) {
    case 'mysql':
      $report->add(_advstatus_report_database_mysql($conn));
      break;
  }

  return $report;
}


/**
 * Return the database report.
 */
function _advstatus_report_database($anonymizer) {

  global $databases;

  $report = new AdvStatusReport(t("Database information"));

  $table = new AdvStatusReportTable(t('Databases'));

  $table->addHeader(t('Database'));
  $table->addHeader(t('Target'));
  $table->addHeader(t('Driver'));

  $reports = array();

  $anonymizer->addCommon('database',
                         array('default', 'extra'));
  $anonymizer->addCommon('dbtarget',
                         array('default', 'slave'));

  $i = $j = 0;
  foreach ($databases as $db_name => $database) {

    foreach ($database as $tg_name => $target) {

      $table->add(array(
                    $anonymizer->get('database', $db_name),
                    $anonymizer->get('dbtarget', $tg_name),
                    $target['driver'],
                  ));
      $db_report = _advstatus_report_database_target($db_name,
                                                      $tg_name,
                                                      $target,
                                                      $anonymizer);
      $reports[] = $db_report;

      $j++;
    }
    $i++;
  }

  if (count($table->children) > 1) {
    $report->addChild($table);
  }

  foreach ($reports as $r) {
    $report->addChild($r);
  }

  return $report;
}


/**
 * Return the entities report.
 */
function _advstatus_report_entities($anonymizer) {

  $report = new AdvStatusReport(t("Entities"));

  $table = new AdvStatusReportTable();

  $table->addHeader(t('Name'));
  $table->addHeader(t('Count'), 'r');

  foreach (entity_get_info() as $name => $info) {
    $base_table = $info['base table'];

    $query = db_select($base_table, 'bt')
      ->fields('bt')
      ->countQuery();

    $count = $query->execute()->fetchField();
    $col = $anonymizer->get('entity', $name);

    $table->add(array($col, advstatus_mrange($count)));
  }

  $report->addChild($table);

  return $report;
}


/**
 * Return the main Advanced status report.
 */
function advstatus_report() {

  $report = new AdvStatusReport(t("Advanced status report"));

  $list = new AdvStatusReportList();
  $list->add(t('Drupal version: @version',
               array('@version' => VERSION)));
  $list->add(t('OS: @name @release @version @machine',
               array(
                 '@name' => php_uname('s'),
                 '@release' => php_uname('r'),
                 '@version' => php_uname('v'),
                 '@machine' => php_uname('m'),
               )));
  if (PHP_OS == 'Linux' && is_executable('/usr/bin/lsb_release')) {
    $distro = exec('/usr/bin/lsb_release -sd');
    if ($distro) {
      $list->add(t('Linux distribution: @distro',
                   array('@distro' => $distro)));
    }
  }

  $list->add(t('Server software: @software',
               array('@software' => $_SERVER['SERVER_SOFTWARE'])));
  $list->add(t('PHP version: @version',
               array('@version' => PHP_VERSION)));

  $report->addChild($list);

  $modules = system_get_info('module');

  foreach ($modules as $module => $info) {
    $function = $module . '_advstatus_report';
    if (!function_exists($function)) {
      module_load_include('inc', 'advstatus', "modules/$module");
    }
    module_invoke($module, 'advstatus_report_init', $report);
  }

  // Add default report sections.
  $report->addChild(_advstatus_report_requirements());
  $report->addChild(_advstatus_report_php());
  $report->addChild(_advstatus_report_database($report->anonymizer));
  $report->addChild(_advstatus_report_themes());
  $report->addChild(_advstatus_report_modules());
  $report->addChild(_advstatus_report_entities($report->anonymizer));

  foreach ($modules as $module => $info) {
    $result = module_invoke($module, 'advstatus_report', $report->anonymizer);
    if ($result) {
      if (!$result->title) {
        $result->title = $info['name'];
      }
      $report->add($result);
    }
  }

  return $report;
}
