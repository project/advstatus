<?php
/**
 * @file
 * Implements hooks on behalf of the core Database logging module.
 */

/**
 * Implements hook_advstatus_report().
 */
function dblog_advstatus_report($anonymizer) {
  $report = new AdvStatusReport();

  $phperrors = new AdvStatusReport(t('Most recent PHP errors'));

  $res = db_select('watchdog', 'wd')
    ->fields('wd', array('message', 'variables'))
    ->condition('type', 'php')
    ->range(0, 5)
    ->orderBy('timestamp', 'DESC')
    ->execute();

  $list = new AdvStatusReportList();

  foreach ($res as $row) {
    $vars = unserialize($row->variables);

    $vars['%file'] = advstatus_anon_file($vars['%file']);

    $msg = advstatus_html2md(format_string($row->message, $vars));
    $list->add($msg);
  }

  if ($list->children) {
    $phperrors->add($list);
  }
  else {
    $phperrors->add(t('No PHP errors found.'));
  }

  $report->add($phperrors);

  return $report;
}
