<?php
/**
 * @file
 * Implements hooks on behalf of the core Path module.
 */

/**
 * Implements hook_advstatus_report().
 */
function path_advstatus_report($anonymizer) {
  $report = new AdvStatusReport();

  $list = new AdvStatusReportList();

  $query = db_select('url_alias', 'ua');
  $query->addExpression('COUNT(*)', 'count');

  $res = $query->execute()->fetch();

  $list->add(t('Number of URL aliases: !total',
               array('!total' => advstatus_mrange($res->count + 1))));

  $report->add($list);

  return $report;
}
