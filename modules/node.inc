<?php
/**
 * @file
 * Implements hooks on behalf of the core Node module.
 */

/**
 * Implements hook_advstatus_report().
 */
function node_advstatus_report($anonymizer) {

  $report = new AdvStatusReport();

  $types = node_type_get_types();

  $table = new AdvStatusReportTable();

  $table->title = t('Content types');

  $table->addHeader(t('Name'));
  $table->addHeader(t('Custom'));
  $table->addHeader(t('Module'));
  $table->addHeader(t('Total nodes'), 'r');

  $query = db_select('node', 'n')
    ->fields('n', array('type'));
  $query->addExpression('COUNT(*)', 'count');
  $query->groupBy('n.type');

  $count = $query->execute()->fetchAllAssoc('type');

  foreach ($types as $type => $info) {
    $table->add(array(
                  $anonymizer->get('content_type', $type),
                  $info->custom ? t('Yes') : t('No'),
                  $info->module,
                  advstatus_mrange(isset($count[$type]) ?
                                   $count[$type]->count : 0),
                ));
  }

  $report->addChild($table);

  // Add node access information
  $table = new AdvStatusReportTable();

  $table->title = t('Node access realms');

  $table->addHeader(t('Name'));
  $table->addHeader(t('Grants'), 'r');

  $query = db_select('node_access', 'na')
    ->fields('na', array('realm'));
  $query->addExpression('COUNT(*)', 'count');
  $query->groupBy('na.realm');

  $res = $query->execute();

  foreach ($res as $row) {
       $table->add(array(
                        $row->realm,
                        advstatus_mrange($row->count),
                     ));
  }

  $report->addChild($table);

  return $report;
}


/**
 * Implements hook_advstatus_report_init().
 */
function node_advstatus_report_init($report) {
  $common_types = array(
    'page', 'story', 'article',
    'book', 'blog',
  );

  $report->anonymizer->addCommon('entity', 'node');
  $report->anonymizer->addCommon('bundle', $common_types);
  $report->anonymizer->addCommon('content_type', $common_types);
}
