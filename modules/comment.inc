<?php
/**
 * @file
 * Implements hooks on behalf of the core Comment module.
 */

/**
 * Implements hook_advstatus_report().
 */
function comment_advstatus_report($anonymizer) {

  $report = new AdvStatusReport();

  $table = new AdvStatusReportTable();

  $table->title = t('Comments by content type');

  $table->addHeader(t('Content type'));
  $table->addHeader(t('Comments'), 'r');

  $query = db_select('node', 'n')
    ->fields('n', array('type'));
  $query->join('comment', 'c', 'n.nid = c.nid');
  $query->addExpression('COUNT(*)', 'count');
  $query->groupBy('n.type');

  $res = $query->execute();

  foreach ($res as $row) {
    $table->add(array(
                  $anonymizer->get('content_type', $row->type),
                  advstatus_mrange($row->count),
                  ));
  }

  $report->addChild($table);

  return $report;
}


/**
 * Implements hook_advstatus_report_init().
 */
function comment_advstatus_report_init($report) {
  $report->anonymizer->addCommon('entity', 'comment');
}
