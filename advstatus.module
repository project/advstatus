<?php
/**
 * @file
 * Advanced status module main file.
 *
 * Contains Drupal hook implementations. APIs and page callbacks are
 * kept on separate files.
 */

/**
 * Implements hook_menu().
 */
function advstatus_menu() {
  $items = array();

  $items['admin/reports/status/advstatus'] = array(
    'title' => 'Advanced status report',
    'description' => 'Display and download the advanced status report',
    'type' => MENU_LOCAL_ACTION,
    'page callback' => 'advstatus_support_report_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'advstatus.pages.inc',
  );

  $items['admin/reports/status/advstatus/view'] = array(
    'title' => 'Advanced status report',
    'description' => 'Display the advanced status report',
    'type' => MENU_CALLBACK,
    'page callback' => 'advstatus_support_report_view',
    'access arguments' => array('administer site configuration'),
    'file' => 'advstatus.pages.inc',
  );

  $items['admin/reports/status/advstatus/download'] = array(
    'title' => 'Download',
    'description' => 'Download the advanced status report',
    'type' => MENU_LOCAL_ACTION,
    'page callback' => 'advstatus_support_report_download',
    'access arguments' => array('administer site configuration'),
    'file' => 'advstatus.pages.inc',
  );

  return $items;
}


/**
 * Implements hook_help().
 */
function advstatus_help($path, $arg) {

  switch ($path) {

    case 'admin/help#advstatus':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Advanced status module provides a more detailed status reports that makes more easy for technical support persons assist you with issues and questions about your Drupal site.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('The <a href="!advstatus-report">Advanced status report</a> page display the advanced status report for your Drupal site. This report will contain in-depth information about your Drupal site that may help technical support solve your issues and/or answer your questions better. The report will be displayed on the page, so that you can assess its contents, perhaps copy and paste text in emails or issue queues. Additionally, you can download the report to a text file in your computer to subsequently attach it to emails, or upload elsewhere.',
                          array('!advstatus-report' => url('admin/reports/status/advstatus')))
        . '</p>';
      $output .= '<p>' . t('<strong>Important:</strong> The <em>Advanced status report</em> will not contain any sensitive data about your Drupal installation (such as passwords or other user data). It also will not usually contain data that can identify you and/or your site, such as your site URL, site name, etc. However, it <strong>will contain</strong> some information that you may not want to advertise, such as the version of enabled modules, PHP version, database driver, etc. You must be careful if you advertise <em>Advanced status reports</em> to the public (such as in attachments to Drupal issue queues) if people can relate the report to your sites in any way.') . '</p>';
      return $output;

    case 'admin/reports/status/advstatus':
      return '<p>' . t('The <em>Advanced status report</em> contains more detailed information about your site that will help technical support to provide more precise answers to your issues and/or questions. You may inspect the report and copy and paste the information in e-mails or issue queues. Additionally, you can download the report to a text file on your computer.') . '</p>';

  }
}
