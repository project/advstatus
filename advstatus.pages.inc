<?php
/**
 * @file
 * Page callbacks for the Advanced status report module.
 */

require_once 'advstatus.inc';


/**
 * Page callback: Main report page.
 *
 * This callback return a render array structure containing an IFRAME
 * that points to the actual report callback.
 *
 * @see advstatus_menu()
 */
function advstatus_support_report_page() {
  drupal_set_title(t('Advanced status report'));

  $mypath = drupal_get_path('module', 'advstatus');

  return array(
    'advstatus_iframe' => array(
      '#markup' => '<iframe id="advstatus-report" src="'
      . url('admin/reports/status/advstatus/view') . '"></iframe>',
      '#attached' => array(
        'css' => array(
          "$mypath/advstatus.css",
        ),
      ),
    ),
  );
}


/**
 * Page callback: The Advanced status report.
 *
 * Outputs the actual text report.
 */
function advstatus_support_report_view() {
  drupal_add_http_header('Content-Type', 'text/plain; charset=UTF-8');
  print advstatus_report();
}


/**
 * Page callback: Download link.
 *
 * Setup the response so that the browser (hopefully) will offer to
 * download the report, instead of displaying it.
 */
function advstatus_support_report_download() {
  drupal_add_http_header('Content-Disposition',
                         'attachment; filename="advanced-status-report.txt"');
  advstatus_support_report_view();
}
