<?php
/**
 * @file
 * Documentation for the Advanced status report module public API.
 */

/**
 * Return a module-specific Advanced status report.
 *
 * @param AdvStatusAnonymizer $anon
 *   An "anonymizer" object that can be used to anonymize sensitive
 *   data. See files in the "module" of the Advanced status report
 *   module for usage examples.
 *
 * @return AdvStatusReport
 *   An Advanced status report object.
 */
function hook_advsupport_report(AdvStatusAnonymizer $anon) {

  $report = new AdvStatusReport();

  $table = new AdvStatusReportTable();

  $table->title = t('Date formats');

  $table->addHeader(t('Format'));
  $table->addHeader(t('Value'), 'r');

  $now = time();
  $table->add(array(
                t('Short'),
                format_date($now, 'short'),
              ));
  $table->add(array(
                t('Medium'),
                format_date($now, 'medium'),
              ));
  $table->add(array(
                t('Long'),
                format_date($now, 'long'),
              ));
  $table->add(array(
                $anon->get('dateformat', 'local'),
                format_date($now, 'local'),
              ));

  $report->addChild($table);

  return $report;
}


/**
 * Initialize the main Advanced status report object.
 *
 * @param AdvStatusReport $report
 *   The main report object.
 */
function hook_advstatus_report_init(AdvStatusReport $report) {
  $report->anonymizer->addCommon('dateformat', 'local');
}
